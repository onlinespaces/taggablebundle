<?php

namespace Onlinespaces\TaggableBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Tag
 *
 * @ORM\Entity()
 * @ORM\Table(name="tags")
 */
class Tag
{
    /**
     * @var int $id
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint", unique=true)
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", length=50, unique=true)
     */
    private $name;

    /**
     * @var string $slug
     *
     * @ORM\Column(name="slug", length=128, unique=true)
     */
    private $slug;

    /**
     * @var bool $is_enabled
     *
     * @ORM\Column(name="is_enabled", type="boolean")
     */
    private $is_enabled;

    /**
     * @var \DateTime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime $updatedAt
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var TaggingContainer $taggingContainer
     *
     * @ORM\OneToMany(targetEntity="TaggingContainer", mappedBy="tag", fetch="EAGER")
     */
    private $taggingContainer;

    public function __construct($name = '')
    {
        $this->setName($name);
        $this->setIsEnabled(true);
        $this->setCreatedAt(new \DateTime('now'));
        $this->setUpdatedAt(new \DateTime('now'));
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return bool
     */
    public function getIsEnabled(): bool
    {
        return $this->is_enabled;
    }

    /**
     * @param bool $is_enabled
     */
    public function setIsEnabled(bool $is_enabled)
    {
        $this->is_enabled = $is_enabled;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return TaggingContainer
     */
    public function getTaggingContainer(): TaggingContainer
    {
        return $this->taggingContainer;
    }

    /**
     * @param TaggingContainer $taggingContainer
     */
    public function setTaggingContainer(TaggingContainer $taggingContainer)
    {
        $this->taggingContainer = $taggingContainer;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }
}