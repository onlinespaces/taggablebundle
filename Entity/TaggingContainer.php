<?php

namespace Onlinespaces\TaggableBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Onlinespaces\TaggableBundle\Doctrine\TaggableInterface;

/**
 * Class TaggingContainer
 *
 * @ORM\Entity
 * @ORM\Table(name="tagging_containers", uniqueConstraints={@UniqueConstraint(name="tagging_container_idx", columns={"id", "entity_type", "entity_id"})})
 */
class TaggingContainer
{
    /**
     * @var string $id
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint", unique=true)
     */
    protected $id;

    /**
     * @var Tag $tag
     *
     * @ORM\ManyToOne(targetEntity="Tag", inversedBy="taggingContainer")
     * @ORM\JoinColumn(name="tag_id", referencedColumnName="id")
     */
    protected $tag;

    /**
     * @var string $entityId
     *
     * @ORM\Column(name="entity_id")
     */
    protected $entityId;

    /**
     * @var string $entityType
     *
     * @ORM\Column(name="entity_type")
     */
    protected $entityType;

    /**
     * TaggingContainer constructor.
     *
     * @param Tag|null $tag
     * @param TaggableInterface|null $entity
     */
    public function __construct(Tag $tag = null, TaggableInterface $entity = null)
    {
        if(null !== $tag) {
            $this->setTag($tag);
        }

        if(null !== $entity) {
            $this->setEntity($entity);
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Tag
     */
    public function getTag(): Tag
    {
        return $this->tag;
    }

    /**
     * @param Tag $tag
     */
    public function setTag(Tag $tag)
    {
        $this->tag = $tag;
    }

    /**
     * Gets the entity type that is tagged
     *
     * @return string
     */
    public function getEntityType(): string
    {
        return $this->entityType;
    }

    /**
     * @param string $entityType
     */
    public function setEntityType(string $entityType)
    {
        $this->entityType = $entityType;
    }

    /**
     * Gets the entity id that is tagged
     *
     * @return string
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @param string $entityId
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;
    }

    /**
     * @param TaggableInterface $entity
     */
    public function setEntity(TaggableInterface $entity)
    {
        $this->setEntityType($entity->getTaggableType());
        $this->setEntityId($entity->getTaggableId());
    }
}