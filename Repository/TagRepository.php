<?php

namespace Onlinespaces\TaggableBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Onlinespaces\TaggableBundle\Entity\Tag;

/**
 * Class TagRepository
 */
class TagRepository extends EntityRepository
{
    /**
     * @var string $queryField
     */
    private $queryField = 'name';

    /**
     * @param string $taggableType      The taggable/entity type
     * @param null|integer $limit       The max amount of results to return
     * @return array
     */
    public function getTagsWithCountArray($taggableType, $limit = null)
    {
        $builder = $this->getTagsWithCountArrayQueryBuilder($taggableType);

        if(null !== $limit) {
            $builder->setMaxResults($limit);
        }

        $results = $builder->getQuery()
          ->getResult(AbstractQuery::HYDRATE_SCALAR);

        $tags = array();

        foreach ($results as $result) {
            $count = $result['tag_count'];

            if($count > 0) {
                $name = $result[$this->queryField];
                $tags[$name] = $count;
            }
        }

        return $tags;
    }

    /**
     * Returns an array of ids (e.g. Article ids) for a entity type that has
     * the given tag
     *
     * @param string $taggableType
     * @param string $tag
     * @return array
     */
    public function getEntityIdsForTag($taggableType, $tag)
    {
        $results = $this->getTagsQueryBuilder($taggableType)
          ->andWhere('tag.' . $this->queryField . ' = :tag')
          ->setParameter('tag', $tag)
          ->select('tagging_container.entityId')
          ->getQuery()
          ->execute(array(), AbstractQuery::HYDRATE_SCALAR);

        $ids = array();

        foreach ($results as $result) {
            $ids[] = $result['entityId'];
        }

        return $ids;
    }

    /**
     * @param string $entityId
     * @param Tag $tag
     * @return mixed
     */
    public function getEntityTypeForTagAndEntityId($entityId, $tag) {
        $result = $this->createQueryBuilder('t')
          ->select('t')
          ->from('t.taggingContainer', 't')
          ->where('t.tagId = :type')
          ->setParameter('type', $tag->getId())
          ->andWhere('t.entityId = :id')
          ->setParameter('is', $entityId)
          ->getQuery()
          ->getOneOrNullResult();

        return $result;
    }

    /**
     * @param string $taggableType
     * @return QueryBuilder
     */
    public function getTagsWithCountArrayQueryBuilder($taggableType) :QueryBuilder
    {
        return $this->getTagsQueryBuilder($taggableType)
          ->groupBy('tagging_container.tag')
          ->select('tag.' . $this->queryField . ', COUNT(tagging_container.tag) AS tag_count')
          ->orderBy('tag_count', 'DESC');
    }

    /**
     * @param string $taggableType
     * @return QueryBuilder
     */
    public function getTagsQueryBuilder($taggableType) : QueryBuilder
    {
        return $this->createQueryBuilder('tag')
          ->join('tag.tagging_container', 'tagging_container')
          ->where('tagging_container.entity_type = :entityType')
          ->setParameter('entity_type', $taggableType);
    }
}