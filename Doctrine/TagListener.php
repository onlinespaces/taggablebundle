<?php
namespace Onlinespaces\TaggableBundle\Doctrine;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

/**
 * Class TagListener
 */
class TagListener implements EventSubscriber
{
    /**
     * @var TagManager
     */
    private $manager;

    /**
     * TagListener constructor.
     * @param TagManager $manager
     */
    public function __construct(TagManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents()
    {
        return array(Events::preRemove);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preRemove(LifecycleEventArgs $args)
    {
        if (($entity = $args->getObject()) and $entity instanceof TaggableInterface) {
            $this->manager->deleteTaggingContainer($entity);
        }
    }
}