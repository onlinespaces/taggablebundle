<?php

namespace Onlinespaces\TaggableBundle\Doctrine;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr;
use Onlinespaces\TaggableBundle\Entity\Tag;
use Onlinespaces\TaggableBundle\Entity\TaggingContainer;

/**
 * Class TagManager
 */
class TagManager
{
    /**
     * @var string
     */
    private $slugger;

    /**
     * @var EntityManager $manager
     */
    protected $manager;

    /**
     * @var string $tagClass
     */
    protected $tagClass;

    /**
     * @var string $taggingContainerClass
     */
    protected $taggingContainerClass;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     * @param null|string $tagClass
     * @param null $taggingContainerClass
     * @param \TaggableBundle\Doctrine\SluggerInterface $slugger
     * @internal param \Doctrine\ORM\EntityManager $manager
     * @internal param null|string $taggingClass
     */
    public function __construct(EntityManager $em, $tagClass = null, $taggingContainerClass = null, SluggerInterface $slugger)
    {
        $this->manager = $em;
        $this->tagClass = $tagClass ?: 'TaggableBundle\Entity\Tag';
        $this->taggingContainerClass = $taggingContainerClass ?: 'TaggableBundle\Entity\TaggingContainer';
        $this->slugger = $slugger;
    }

    /**
     * Adds a tag to the taggable entity
     *
     * @param Tag $tag
     * @param TaggableInterface $entity
     */
    public function addTag(Tag $tag, TaggableInterface $entity)
    {
        $tags = $entity->getTags();
        $tags->add($tag);
    }

    /**
     * Add multiple tags to a taggable entity
     *
     * @param Tag[] $tags
     * @param TaggableInterface $entity
     */
    public function addTags(array $tags, TaggableInterface $entity)
    {
        foreach ($tags as $tag) {
            if($tag instanceof Tag) {
                $this->addTag($tag, $entity);
            }
        }
    }

    /**
     * Removes a tag on a taggable entity
     *
     * @param \TaggableBundle\Entity\Tag $tag
     * @param \TaggableBundle\Doctrine\TaggableInterface $entity
     * @return bool
     */
    public function removeTag(Tag $tag, TaggableInterface $entity) {
        return $entity->getTags()->removeElement($tag);
    }

    /**
     * @param Tag[] $tags
     * @param TaggableInterface $entity
     */
    public function replaceTags(array $tags, TaggableInterface $entity)
    {
        $entity->getTags()->clear();
        $this->addTags($tags, $entity);
    }

    /**
     * @param string $name
     * @return Tag
     */
    public function loadOrCreateTag($name)
    {
        $tags = $this->loadOrCreateTags(array($name));
        return $tags[0];
    }

    /**
     * @param array $names
     * @return Tag[]
     */
    public function loadOrCreateTags(array $names)
    {
        if(empty($names)) {
            return array();
        }

        $names = array_unique($names);

        $builder = $this->manager->createQueryBuilder();

        $tags = $builder
          ->select('t')
          ->from($this->tagClass, 't')
          ->where($builder->expr()->in('t.name', $names))
          ->getQuery()
          ->getResult();

        $loadedTagNames = array();

        /** @var Tag $tag */
        foreach ($tags as $tag) {
            $loadedTagNames[] = $tag->getName();
        }

        $missingNames = array_udiff($names, $loadedTagNames, 'strcasecmp');

        if(0 < count($missingNames)) {
            foreach ($missingNames as $name) {
                $tag = $this->createTag($name);
                $this->manager->persist($tag);

                $tags[] = $tag;
            }

            $this->manager->flush();
        }
        return $tags;
    }

    /**
     * Save tags for a taggable entity
     *
     * @param TaggableInterface $entity
     */
    public function saveTaggingContainer(TaggableInterface $entity)
    {
        $currentTags = $this->getTaggingContainer($entity);
        $newTags = $entity->getTags();
        $tagsToAdd = $newTags;

        if($currentTags !== null && is_array($currentTags) && !empty($currentTags)) {
            $tagsToRemove = array();

            foreach ($currentTags as $currentTag) {
                if ($newTags->exists(function ($index, $newTag) use ($currentTag) {
                    return $newTag->getName() == $currentTag->getName();
                })) {
                    $tagsToAdd->removeElement($currentTag);
                } else {
                    $tagsToRemove[] = $currentTag->getId();
                }
            }

            if(count($tagsToRemove) > 0) {
                $builder = $this->manager->createQueryBuilder();

                $builder->delete($this->taggingContainerClass, 't')
                    ->where('t.tag_id')
                    ->where($builder->expr()->in('t.tag', $tagsToRemove))
                    ->andWhere('t.entityType = :entityType')
                    ->setParameter('entityType', $entity->getTaggableType())
                    ->andWhere('t.entityId = :entityId')
                    ->setParameter('entityId', $entity->getTaggableId())
                    ->getQuery()
                    ->getResult();
            }
        }

        foreach ($tagsToAdd as $tag) {
            $this->manager->persist($tag);
            $this->manager->persist($this->createTaggingContainer($tag, $entity));
        }

        if(count($tagsToAdd)) {
            $this->manager->flush();
        }
    }

    /**
     * Loads all of the tags for the taggable entity
     *
     * @param TaggableInterface $entity
     */
    public function loadTaggingContainer(TaggableInterface $entity) {
        $tags = $this->getTaggingContainer($entity);
        $this->replaceTags($tags, $entity);
    }

    /**
     * @param TaggableInterface $entity
     * @return array
     */
    protected function getTaggingContainer(TaggableInterface $entity)
    {
        return $this->manager->createQueryBuilder()
          ->select('t')
          ->from($this->tagClass, 't')
          ->innerJoin('t.taggingContainer', 't2', Expr\Join::WITH, 't2.entityId = :id AND t2.entityType = :type')
          ->setParameter('id', $entity->getTaggableId())
          ->setParameter('type', $entity->getTaggableType())
          ->getQuery()
          ->getResult();
    }

    /**
     * Deletes all taggingContainer records from taggable entity
     *
     * @param TaggableInterface $entity
     */
    public function deleteTaggingContainer(TaggableInterface $entity)
    {
        $taggingContainerList = $this->manager->createQueryBuilder()
          ->select('t')
          ->from($this->taggingContainerClass, 't')
          ->where('t.entityType = :type')
          ->setParameter('type', $entity->getTaggableType())
          ->andWhere('t.entityId = :id')
          ->setParameter('id', $entity->getTaggableId())
          ->getQuery()
          ->getResult();

        foreach ($taggingContainerList as $tagging) {
            $this->manager->remove($tagging);
        }
    }

    /**
     * @param string $names
     * @param string $separator
     *
     * @return string[]|array
     */
    public function splitTagNames($names, $separator = ',')
    {
        $tags = explode($separator, $names);
        $tags = array_map('trim', $tags);
        $tags = array_filter($tags, function ($value) { return !empty($value); });

        return array_values($tags);
    }

    /**
     * @param TaggableInterface $entity
     * @return array
     */
    public function getTagNames(TaggableInterface $entity)
    {
        $names = array();

        if(0 < count($entity->getTags())) {
            foreach ($entity->getTags() as $tag) {
                $names[] = $tag->getName();
            }
        }

        return $names;
    }

    /**
     * @param string $name
     * @return Tag
     */
    protected function createTag($name)
    {
        $tag = new Tag($name);
        $tag->setSlug($this->slugger->slugify($name));
        return $tag;
    }

    /**
     * @param Tag $tag
     * @param TaggableInterface $entity
     * @return TaggingContainer
     */
    protected function createTaggingContainer(Tag $tag, TaggableInterface $entity) {
        return new $this->taggingContainerClass($tag, $entity);
    }
}