<?php

namespace Onlinespaces\TaggableBundle\Doctrine;

use Doctrine\Common\Collections\ArrayCollection;
use Onlinespaces\TaggableBundle\Entity\Tag;

/**
 * Interface TaggableInterface
 */
interface TaggableInterface
{
    /**
     * @return string
     */
    function getTaggableType();

    /**
     * @return string
     */
    function getTaggableId();

    /**
     * @return ArrayCollection|Tag[]
     */
    function getTags();
}