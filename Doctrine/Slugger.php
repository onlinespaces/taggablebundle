<?php

namespace Onlinespaces\TaggableBundle\Doctrine;

/**
 * Class Slugger
 */
class Slugger implements SluggerInterface
{
    /**
     * @param string $name
     * @return string
     */
    function slugify($name)
    {
        $slug = mb_convert_case($name, MB_CASE_LOWER, mb_detect_encoding($name));
        $slug = str_replace(' ', '-', $slug);
        $slug = str_replace('--', '-', $slug);
        return $slug;
    }
}