<?php

namespace Onlinespaces\TaggableBundle\Doctrine;

/**
 * Interface SluggerInterface
 */
interface SluggerInterface
{
    function slugify($name);
}